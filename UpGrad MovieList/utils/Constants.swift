//
//  Constants.swift
//  UpGrad MovieList
//
//  Created by Anuj on 15/01/19.
//  Copyright © 2019 Upgrad. All rights reserved.
//

import Foundation

class Constants {
    
    public static let BASE_URL = "https://api.themoviedb.org/3/movie/"
}
