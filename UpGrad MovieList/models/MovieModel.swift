//
//  MovieModel.swift
//  UpGrad MovieList
//
//  Created by Anuj on 15/01/19.
//  Copyright © 2019 Upgrad. All rights reserved.
//

import Foundation

public struct MoviesModel : Decodable {

    public var page : Int!
    public var results : [Result]!
    public var total_results : Int!
    public var total_pages : Int!

}

public struct Result : Decodable {

    public var adult : Bool!
    public var backdropPath : String?
    public var genreIds : [Int]!
    public var id : Int!
    public var originalLanguage : String!
    public var originalTitle : String!
    public var overview : String!
    public var popularity : Float!
    public var poster_path : String!
    public var releaseDate : String!
    public var title : String!
    public var video : Bool!
    public var voteAverage : Float!
    public var voteCount : Int!

}
