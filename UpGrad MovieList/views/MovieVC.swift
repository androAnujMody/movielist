//
//  MovieVC.swift
//  UpGrad MovieList
//
//  Created by Anuj on 16/01/19.
//  Copyright © 2019 Vedic Rishi. All rights reserved.
//

import UIKit
import Foundation
import RxSwift

class MovieVC: BaseVC,UIActionSheetDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    //MARK:varibles
    var presenter : MoviePresenter?
    var disposeBag : Disposable?
    var model : MoviesModel?
    var page = 1
    var results : [Result]?
    var paths : [IndexPath]?
    var isLoading = false


    @IBAction func btSort(_ sender: Any) {

        let actionSheetController: UIAlertController = UIAlertController(title: "Sort By", message: "", preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in

        }
        actionSheetController.addAction(cancelActionButton)

        let saveActionButton = UIAlertAction(title: "Popular", style: .default)
        { _ in
            self.reloadFromStart(type: "popular")
        }
        actionSheetController.addAction(saveActionButton)

        let deleteActionButton = UIAlertAction(title: "Higest Rated", style: .default)
        { _ in
            self.reloadFromStart(type: "top_rated")
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }



    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        loadData()

    }

    func setUpViews() {

        self.collectionView.isHidden=true
        self.collectionView.register(UINib(nibName: "MovieListCVC", bundle: nil), forCellWithReuseIdentifier: "MovieListCVC")
        self.collectionView.delegate=self;
        self.collectionView.dataSource=self;

    }

    func loadData()  {
        presenter = MoviePresenter()
        presenter?.array.onNext(["popular","1"])
        presenter?.getMovies().observeOn(MainScheduler.instance).subscribe(onNext:{ [weak self]  (json) in
            //self?.model = json
            self?.collectionView.isHidden=false;
            self?.activityIndicator.isHidden = true
            if(self?.results==nil){
                self?.results = json.results
                self?.collectionView.reloadData();
            }else{
                self?.paths = [IndexPath]()
                let preSize = self?.results?.count
                for i in 0..<json.results.count {
                    let indexPath = IndexPath(row: i + preSize!, section: 0)
                    self?.paths?.append(indexPath)
                    self?.results?.append(json.results[i])
                }

                self?.collectionView.insertItems(at:(self?.paths)!)
                self?.isLoading = false
            }


            },onError:{
                [weak self] err in
                print("error getting data")

        })
    }

    func reloadFromStart(type:String){
        isLoading = false
        page = 1
        results?.removeAll()
        results = nil
        collectionView.isHidden = true
        activityIndicator.isHidden = false
        presenter?.array.onNext([type, "\(page)"])
    }

}
extension MovieVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results?.count ?? 0;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "MovieListCVC", for: indexPath) as! MovieListCVC;
        cell.data = self.results?[indexPath.row]
        return cell;
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "MovieDetailSB", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailVC") as! MovieDetailVC
        vc.movieTitle = results?[indexPath.row].title
        vc.desc = results?[indexPath.row].overview
        vc.image = results?[indexPath.row].poster_path
        self.present(vc, animated: true, completion: nil);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width/2 - 8, height: 200)
    }
    


    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsety = scrollView.contentOffset.y
        if contentOffsety >= (scrollView.contentSize.height - scrollView.bounds.height) && !isLoading  {

            isLoading = true
            page = page + 1
            presenter?.array.onNext(["popular", "\(page)"])
        }
    }
    
    
}
