//
//  MovieListVC.swift
//  UpGrad MovieList
//
//  Created by Anuj on 16/01/19.
//  Copyright © 2019 Upgrad. All rights reserved.
//

import UIKit
import Nuke

class MovieDetailVC: UIViewController {
    
    @IBOutlet weak var bt_back: UIButton!
    @IBOutlet weak var tv_description: UITextView!
    @IBOutlet weak var lb_movieName: UILabel!
    @IBOutlet weak var img_poster: UIImageView!
    
    var movieTitle:String?
    var desc:String?
    var image:String?
    
    @IBAction func bt_back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lb_movieName.text = movieTitle ?? ""
        tv_description.text = desc ?? ""
        
        if(image != nil){
            let url = URL(string: "https://image.tmdb.org/t/p/w92\(image!)")
            Nuke.loadImage(with: url!, into: img_poster)
        }
        
        
    }
    
}
