//
//  MovieListCVC.swift
//  UpGrad MovieList
//
//  Created by Anuj on 16/01/19.
//  Copyright © 2019 Vedic Rishi. All rights reserved.
//

import UIKit
import Nuke



class MovieListCVC: UICollectionViewCell {

    @IBOutlet weak var img_movie: UIImageView!
    
    @IBOutlet weak var lb_movieName: UILabel!

    var data : Result?{
        didSet{
            lb_movieName.text = data?.title
            //            Nuke.loadImage(with: NSURL(string: "https://image.tmdb.org/t/p/original\(data?.poster_path!)"), into: img_movie)

            if(data?.poster_path != nil){
                let url = NSURL(string: "https://image.tmdb.org/t/p/w92\(data!.poster_path!)")! as URL
                Nuke.loadImage(with: url, into: img_movie)
            }




        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
