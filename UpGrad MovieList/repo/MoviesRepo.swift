//
//  MoviesRepo.swift
//  UpGrad MovieList
//
//  Created by Anuj on 15/01/19.
//  Copyright © 2019 Upgrad. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class MoviesRepo {
    
    
    //    func getMovies(type:String,page:Int) -> Observable<MoviesModel> {
    //
    //        return Observable.create{ observer in
    //
    //            Alamofire.request(Constants.BASE_URL+"\(type)?api_key=ae2070ea318cd7dbf0e3094c3ac4f291&page=\(page)").responseJSON { response in
    //                //debugPrint(response)
    //                do{
    //                    let data = try JSONDecoder().decode(MoviesModel.self, from: response.data!)
    //                     observer.onNext(data)
    //                }catch let error as NSError{
    //                    print(error.localizedDescription)
    //                    observer.onError()
    //                }
    //            }
    //        }
    //
    //    }
    
    func getMovies(type:String,page:String) -> Observable<MoviesModel> {
        print(page)
        return Observable.create { observer -> Disposable in
            Alamofire.request(Constants.BASE_URL+"\(type)?api_key=ae2070ea318cd7dbf0e3094c3ac4f291&page=\(page)")
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        guard response.data != nil else {
                            observer.onError(response.error!)
                            return
                        }
                        do{
                            let data = try JSONDecoder().decode(MoviesModel.self, from: response.data!)
                            observer.onNext(data)
                        }catch let error as NSError{
                            observer.onError(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            
            return Disposables.create()
        }
    }

    func getDescription(type:String,page:String) -> Observable<MoviesModel> {
        print(page)
        return Observable.create { observer -> Disposable in
            Alamofire.request(Constants.BASE_URL+"\(type)?api_key=ae2070ea318cd7dbf0e3094c3ac4f291&page=\(page)")
                .validate()
                .responseJSON { response in
                    switch response.result {
                    case .success:
                        guard response.data != nil else {
                            observer.onError(response.error!)
                            return
                        }
                        do{
                            let data = try JSONDecoder().decode(MoviesModel.self, from: response.data!)
                            observer.onNext(data)
                        }catch let error as NSError{
                            observer.onError(error)
                        }
                    case .failure(let error):
                        observer.onError(error)
                    }
            }

            return Disposables.create()
        }
    }
    
}


