//
//  MoviePresenter.swift
//  UpGrad MovieList
//
//  Created by Anuj on 16/01/19.
//  Copyright © 2019 Upgrad. All rights reserved.
//

import Foundation
import RxSwift

class MoviePresenter : MoviesContract {

    var repo : MoviesRepo
    var array : BehaviorSubject<[String]>


    init() {
        repo = MoviesRepo()
        array = BehaviorSubject(value: [""])
    }

    func getMovies() -> Observable<MoviesModel> {
       return array.map { value in
            return self.repo.getMovies(type: value[0], page:value[1])
        }.switchLatest()

    }

}

protocol MoviesContract {
    func getMovies() -> Observable<MoviesModel>
}
